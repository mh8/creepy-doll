# Creepy Doll

A doll hacked with leds instead of eyes and a toggle switch to turn it ON/OFF.

When ON, a sensor detects if the doll is moved and the doll's eyes blink three times.

# Hardware

The code runs on an attiny85.

Two leds are connected to Pin 0 for the eyes.

# Current version

Currently, the code simply blinks two leds connected to pin 0.

# Planned implements

- A toggle switch to turn it ON/OFF
- A sensor pin
