/* mh8
 * A doll blinks two leds in place of her eyes
**/
int led = 0; // LED pin
int brightness = 0;
int fadeAmount = 5; // points to fade LED by

void setup()
{
  pinMode(led, OUTPUT); // init led
}

void loop()
{
  fadeEyes(); // blink the eye thing
}

void fadeEyes()
{
  analogWrite(led, brightness);
  if (brightness == 255)
  {
    delay(1250); // stay a bit on full bright led
  }

  brightness = brightness + fadeAmount;

  //reverse direction at start & end of fade
  if (brightness == 0 || brightness == 255)
  {
    fadeAmount = -fadeAmount;
  }

  // wait 30 miliseconds to see changes

  delay(30);
}
